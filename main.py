#!/usr/bin/env python
"""
URL 'Shortening' service - doesn't shorten, 
Names are case-sensitive! bbc <> BBC
allows user-defined memorable names, 
users redirects to 'vote'
rotted links are reclaimed in time
to support mutiple service sign on
"""

import jinja2
import hashlib
import os
import webapp2
from datetime import datetime
import urlparse
import logging

from google.appengine.ext import db
from google.appengine.api import search
from google.appengine.api import users
from google.appengine.api import taskqueue
from google.appengine.api import memcache
from google.appengine.ext.webapp import template

from gaesessions import get_current_session

import oauth

_INDEX_NAME = 'links'
EXPIRES = 8*60
PAGE_SIZE = 20
DAY_SCALE = 4
TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')
jinja_environment = jinja2.Environment(autoescape=True, extensions=['jinja2.ext.autoescape'], loader=jinja2.FileSystemLoader(TEMPLATE_DIR))

#federated logins
PROVIDERS = {
    'google'   : 'https://www.google.com/accounts/o8/id',
    'yahoo'    : 'yahoo.com',
    'myspace'  : 'myspace.com',
    'aol'      : 'aol.com',
    'myopenid' : 'myopenid.com',
    'twitter'  : '/admin/login?oauth=twitter'
}

#oauth providers
OAUTH_SETTINGS = {
    'twitter': {
        'consumer_key': 'UGKXsK7gNLd5Ei79Jb6qVw',
        'consumer_secret': 'vUJSSnh7YZ43wAFUFcv8EcOGJknAPH84056KmG49Zh4',
        },
    }
    
def user_required(func, *args, **kw):
    '''So we can decorate any RequestHandler with @user_required'''
    def wrapper(self):
        self.session = get_current_session()
        self.user = users.get_current_user()
        if self.user:
            self.login = users.create_logout_url('/')
        else:
            if self.session.has_key('user') and self.session.has_key('auth_service'):
                self.user = self.session['user']
                self.login = '/admin/logout/oauth'
            else:
                return self.redirect('/admin/login')
        func(self, *args, **kw)
    return wrapper

class Links(db.Model):

    text = db.StringProperty(required = True)
    uri = db.StringProperty(required = True)
    creator = db.UserProperty()
    tags = db.StringProperty()
    rank = db.StringProperty()
    votesum = db.IntegerProperty(default=1)
    created = db.IntegerProperty(default=0)
    creation_order = db.StringProperty(default=" ")
    last_update = db.DateTimeProperty(auto_now=True)
    pub_date = db.DateTimeProperty(auto_now_add=True)

class BaseHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def jinja2(self):
        return jinja2.get_jinja2(app=self.app)

    def render_template(
        self,
        filename,
        template_values,
        **template_args
        ):
        template = jinja_environment.get_template(filename)
        self.response.out.write(template.render(template_values))

class TaskHandler(BaseHandler):
    """
    Perform later after link redirection for speeeeed.
    """    
    def post(self, name):        
        logging.info('Running POST on ' + name)
        link = get_link(name)
        if link:
            link.votesum += 1
            link.rank = "%020d|%s" % (long(link.created * DAY_SCALE + link.votesum),link.creation_order)
            db.put(link)
            
            if not memcache.set(name, link, EXPIRES):
                logging.Error('Failed to add %s to memcache' % name)
            
class AdminHandler(BaseHandler):
   
    def get(self, action, name):
        logging.info('GET action:' + action + ', key: ' + name) # yuk!
        user = users.get_current_user()
        if action == 'del':
            logging.info('Request link deletion')
            del_link(name, user)
        elif action == 'reindex':
            if users.is_current_user_admin() or user.nickname().split('@')[1]=='z4p.it':
                delete_all_in_index('links')
                links = Links.all().order('-rank')
                for link in links:
                    name = link.key().name()
                    author = link.creator.nickname()
                    doc = create_document(name, link.text, link.uri, link.votesum, author, link.pub_date)
                    search.Index(name=_INDEX_NAME).put(doc)
            else:
                logging.error('No you are not r00t!')
        elif action == 'logout':
            session = get_current_session()
            session.terminate()
            
        return webapp2.redirect('/')
        
class RedirectHandler(BaseHandler):

    def get(self, name):
        link = get_link(name)
        if link: #TODO check why params no wok 
            taskqueue.add(url='/admin/stats/%s' % name, params={}) # even if link not exist?
            return webapp2.redirect(str(link.uri))
        else:
            #TODO LMGTFY do Google I feel lucky on *name* redirect there instead
            return webapp2.redirect('/')

class LoginHandler(BaseHandler):

    def get(self):
    
        user = users.get_current_user()
        if user:  # signed in already
            return webapp2.redirect('/')
        else:     
            session = get_current_session()
            #on callout
            service = self.request.get("oauth")
            if service:
                logging.info('callout Login %s' % service)
                session['auth_service'] = service
                client = oauthclient(service)
                login_url = client.get_authorization_url()
                logging.info('Login %s' % login_url)
                return webapp2.redirect(login_url)
            
            #on callback
            auth_token = self.request.get("oauth_token")
            auth_verifier = self.request.get("oauth_verifier")
            if auth_token and auth_verifier:
                client = oauthclient(session['auth_service'])
                user_info = client.get_user_info(auth_token, auth_verifier=auth_verifier)
                user = users.User("%s@%s.com" % (str(user_info['username']), str(user_info['service'])))
                session['user'] = user
                logging.info('callback Login %s' % user)
                return webapp2.redirect('/')
            
        # let user choose authenticator
        login = []
        href = '<a href="%s"><img src="../static/images/%sB.png"></img></a>'
        for name, uri in PROVIDERS.items():
            if 'oauth' in uri: #TODO get consumer key for fb & twitter (dropbox, instagram?)
                login.append(href % (uri, name))
            else:
                login.append(href % (users.create_login_url(federated_identity=uri), name))
        self.render_template('login.html', {'login' : login})

class MainHandler(BaseHandler):

    @user_required
    def get(self):
        links = []
        user = self.user
        login = self.login
        query = self.request.get('query')
        logging.info('Search %s' % query)
        
        name = self.request.get('name').strip()
        text = self.request.get('text').strip()      
        uri = self.request.get('uri').strip()
        
        template_values  = {
            'login': login,
            'user': user,
            'uri': uri,
            'text': text,
            'name': name,
            'nexturi': None,
            'prevuri': None
        }
        
        if query:
            results = find_documents(query, 20, search.Cursor())
            if results:
                for s in results:
                    index = 1 
                    links.append({
                        'name': s.fields[0].value,
                        'uri': s.fields[1].value,
                        'text': s.expressions[0].value,
                        'index': index
                    })
                    index += 1
            template_values ['section'] = 'results'
            template_values ['links'] = links
            self.render_template('section.html', template_values)
        else:            
            page = 0
            
            links = Links.gql('ORDER BY rank DESC').fetch(10)
            recent = Links.gql('ORDER BY creation_order DESC').fetch(10)
            trendy = Links.gql('ORDER BY last_update DESC').fetch(10)

            template_values['links'] = links_for_template(links, page=0)
            template_values['new'] = links_for_template(recent, page=0)
            template_values['hot'] = links_for_template(trendy, page=0) #TODO how?

            if user:
                owned = Links.gql('WHERE creator = :1 ORDER BY last_update DESC', user).fetch(10)
                template_values['you'] = links_for_template(owned, page=0)
            
            self.render_template('index.html', template_values)

    @user_required
    def post(self):
        """Add a link to the system."""
        
        logging.info('Posting new link')
        
        user = self.user
        login = self.login
        name = self.request.get('name').strip()
        text = self.request.get('text').strip()      
        uri = self.request.get('uri').strip()
        
        if user:                            
            parsed_uri = urlparse.urlparse(uri)

            template_values = {
                'name': name,
                'user': user,
                'text' : text,
                'uri' : uri,
            }
            if uri and ( not parsed_uri.scheme or not parsed_uri.netloc ):
                template_values['error_msg'] ='The supplied link is not a valid URI'
            else:
                link = get_link(name)
                if link is None or (users.is_current_user_admin() or link.creator == user):
                    link_id = add_link(name, text, user, uri=uri)
                    if link_id is None:
                        template_values['error_msg'] = 'An error occured while adding this link, please try again.'
                        self.render_template('index.html', template_values)
                    else:
                        logging.info('Redirecting back - success!')
                        return self.redirect('/')
                else:
                    template_values = { 'error_msg' : 'Sorry, this link is currently in use.' }
                    return webapp2.redirect('/')
                    
        else:
            template_values = { 'error_msg' : 'Aargh! You need to login again.' }
            return webapp2.redirect('/')

class SectionHandler(BaseHandler):

    @user_required
    def get(self):
        
        page = int(self.request.get('p', '0'))
        section = self.request.get('section')
        logging.info('SECTION %s P %s' % (section, self.request.get('p')))
        
        if section == 'yours':
            links, next = get_links_owned(self.user, page)
        elif section == 'recent':
            links, next = get_links_newest(page)
        elif section == 'trendy':
            links, next = get_links_trending(page)
        else:
            links, next = get_links(page)
            
        if next:
          nexturi = '/links?section=%s&p=%d' % (section, page + 1)
        else:
          nexturi = None
        if page > 1:
          prevuri = '/links?section=%s&p=%d' % (section, page - 1)
        elif page == 1:
          prevuri = '/links?section=%s' % section
        else:
          prevuri = None
        
        template_values  = {
            'login': self.login,
            'user': self.user,
            'links': links_for_template(links, page),
            'section': section,
            'nexturi': nexturi,
            'prevuri': prevuri
        }
        
        self.render_template('section.html', template_values)

def oauthclient(service):

    consumer_key = OAUTH_SETTINGS[service]['consumer_key']
    consumer_secret = OAUTH_SETTINGS[service]['consumer_secret']
    if 'Development' in os.environ['SERVER_SOFTWARE']:
        callback = "http://localhost:8080/admin/login"
    else:
        callback = "http://z4p-it.appspot.com/admin/login"    

    if service == 'twitter':
        return oauth.TwitterClient(consumer_key, consumer_secret, callback)
    elif service == 'dropbox':
        return oauth.DropboxClient(consumer_key, consumer_secret, callback)
    elif service == 'linkedin':
        return oauth.LinkedInClient(consumer_key, consumer_secret, callback)
    else:
        return oauth.YammerClient(consumer_key, consumer_secret, callback)
            
def links_for_template(links, page=0):
  """Convert a Link object into a suitable dictionary for 
  a template. Does some processing on parameters and adds
  an index for paging.
  
  Args
    links:  A list of Link objects.
  
  Returns
    A list of dictionaries, one per Link object.
  """
  links_tpl = []
  index = 1 + page * PAGE_SIZE
  for link in links:
    links_tpl.append({
      'name': link.key().name(), 
      'uri': link.uri,
      'text': link.text,
      'creator': link.creator,
      'created': link.creation_order[:10],
      'created_long': link.creation_order[:19],
      'votesum': link.votesum,
      'index':  index        
    })
    index += 1
  return links_tpl

def uniqify(index):
    seen = set()
    seen_add = seen.add
    return [ x for x in index if x not in seen and not seen_add(x)]

#TODO check sharding correctly
def _unique_user(user):
    now = datetime.now()
    count = (now - datetime(2013, 1, 17)).days # days since aaron
    return hashlib.md5(user.email() + "|" + str(count)).hexdigest()

# search documents (store evil stuff here later mwahaha)
def create_document(name, text, uri, votes, user, pubdate):
    return search.Document(
        fields=[search.TextField(name='name', value=name),
                search.TextField(name='text', value=text),
                search.TextField(name='uri', value=uri),
                search.NumberField(name='votes', value=votes),
                search.TextField(name='creator', value=user),
                search.DateField(name='pubdate', value=pubdate)])

def find_documents(query_string, limit, cursor):
    try:
        subject_desc = search.SortExpression(
            expression='name',
            direction=search.SortExpression.DESCENDING,
            default_value='')

        # Sort up to 20 pages matching results by name in descending order
        sort = search.SortOptions(expressions=[subject_desc], limit=500)

        # Set query options
        options = search.QueryOptions(
            limit=limit,  # the number of results to return
            cursor=cursor,
            sort_options=sort,
            returned_fields=['name', 'uri', 'votes', 'creator', 'pubdate'],
            snippeted_fields=['text'])

        query = search.Query(query_string=query_string, options=options)

        index = search.Index(name=_INDEX_NAME)

        # Execute the query
        return index.search(query)
    except search.Error:
        logging.exception('Search failed')
    return None

def delete_all_in_index(index_name):
    """Delete all the docs in the given index."""
    doc_index = search.Index(name=index_name)

    while True:
        # Get a list of documents populating only the doc_id field and extract the ids.
        document_ids = [document.doc_id
                        for document in doc_index.list_documents(ids_only=True)]
        if not document_ids:
            break
        # Remove the documents for the given ids from the Index.
        doc_index.remove(document_ids)

def add_link(name, text, user, uri=None, _created=None):
    try:
        logging.info('Saving ' + name)
        now = datetime.now()
        unique_user = _unique_user(user)
        if _created:
            created = _created
        else:
            created = (now - datetime(2013, 1, 17)).days
        link = Links(
          key_name=name, #need to encode this!
          text=text, 
          created=created, 
          creator=user, 
          creation_order = now.isoformat()[:19] + "|" + unique_user,
          uri=uri
        )
        votes = 0 # for now
        
        # TODO store comments as a search documents ONLY?
        try:
            search.Index(name=_INDEX_NAME).put(create_document(name, text, uri, votes, user.nickname(), datetime.now().date()))
        except search.Error:
            logging.exception('Put failed')

        try:
            memcache.add(name, link, EXPIRES)
        except:
            logging.exception('Memcache FAIL')

        link.put()
        return link.key()
    except db.Error, exStr:
        logging.error(exStr)
        return None 

def del_link(name, user):
    """
    Remove a link.
    
    User must be the creator of the link or a site administrator.
    """
    link = get_link(name)
    if link is not None and (users.is_current_user_admin() or link.creator == user):
        logging.info('Deleting ' + name + ' for ' + link.creator.nickname())
        link.delete()

"""
def get_link(name):
    key = db.Key.from_path('Links', name)
    link = db.get(key)
    return link
"""
    
def get_link(name):
    """
    Retrieve a single link.
    """
    link = memcache.get(name)
    if link is None:
        key = db.Key.from_path('Links', name)
        link = db.get(key)
        return link
    else:
        return link       

#TODO DRY 
def get_links_newest(page=0):
    """Returns PAGE_SIZE links per page in rank order. Limit to 20 pages."""
    assert page >= 0
    assert page < 20
    extra = None
    links = Links.gql('ORDER BY creation_order DESC').fetch(PAGE_SIZE+1, page*PAGE_SIZE)
    if len(links) > PAGE_SIZE:
        if page < 19:
            extra = links[-1]
        links = links[:PAGE_SIZE]
    return links, extra

def get_links_trending(page=0):
    """Returns PAGE_SIZE links per page in rank order. Limit to 20 pages."""
    assert page >= 0
    assert page < 20
    extra = None
    links = Links.gql('ORDER BY last_update DESC').fetch(PAGE_SIZE+1, page*PAGE_SIZE)
    if len(links) > PAGE_SIZE:
        if page < 19:
            extra = links[-1]
        links = links[:PAGE_SIZE]
    return links, extra

def get_links_owned(user, page=0):
    """Returns PAGE_SIZE links per page in recent order. Limit to 20 pages."""
    assert page >= 0
    assert page < 20
    extra = None
    links = Links.gql('WHERE creator = :1 ORDER BY last_update DESC', user).fetch(PAGE_SIZE+1, page*PAGE_SIZE)
    if len(links) > PAGE_SIZE:
        if page < 19:
            extra = links[-1]
        links = links[:PAGE_SIZE]
    return links, extra
 
def get_links(page=0):
    """Returns PAGE_SIZE links per page in rank order. Limit to 20 pages."""
    assert page >= 0
    assert page < 20
    extra = None
    links = Links.gql('ORDER BY rank DESC').fetch(PAGE_SIZE+1, page*PAGE_SIZE)
    if len(links) > PAGE_SIZE:
        if page < 19:
            extra = links[-1]
        links = links[:PAGE_SIZE]
    return links, extra

app = webapp2.WSGIApplication([
    ('/admin/login', LoginHandler),
    ('/admin/stats/(.*)', TaskHandler),
    ('/admin/(.*)/(.*)', AdminHandler),
    ('/links', SectionHandler),
    ('/', MainHandler),
    ('/(.*)', RedirectHandler),
], debug=True)

