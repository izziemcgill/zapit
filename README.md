:zap:Z4p.it
=======

Shrink-ray for urls.  URL Shortener service hosted on the Google App Engine.  

<a href="http://appengine.google.com/">
  <img src="https://avatars2.githubusercontent.com/u/2810941?s=140">
</a>

URLs are user-defined not hashes, so they can be shared verbally/remembered.  

Example:

http://z4p.it/twitter (links to twitter)

URLs rot through disuse so can be reclaimed eventually.  Needs a landing page for lurkers.  

http://z4p.it redirects to http://z4p-it.appspot.com/ 


